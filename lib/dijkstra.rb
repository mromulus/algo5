require 'set'

Dijkstra = Struct.new(:file) do
  def compute!
    parse

    dijkstra(1)
  end

  def dijkstra(s)
    # @computed_nodes = Set.new
    @path_lengths = Hash.new { |h, key| h[key] = Float::INFINITY }
    @path_lengths[s] = 0

    until @data.empty?
      u = nil;
      @data.keys.each do |min|
        if (!u) || (@path_lengths[min] && @path_lengths[min] < @path_lengths[u])
          u = min
        end
      end

      break if (@path_lengths[u] == Float::INFINITY)

      @data[u].keys.each do |v|
        next_path = @path_lengths[u] + @data[u][v]
        @path_lengths[v] = next_path if (next_path < @path_lengths[v])
      end
      @data.delete u
    end
  end

  def print_all_paths
    @path_lengths.each do |dest, length|
      puts "#{dest} - #{length}"
    end
  end

  def print_interesting_paths
    int = [7,37,59,82,99,115,133,165,188,197]
    @path_lengths.each do |dest, length|
      puts "#{dest} - #{length}" if int.include?(dest)
    end
  end

  private

  def parse
    @data = {}
    file.each do |line|
      vertex, neighbors = line.split(" ", 2)
      next unless neighbors
      @data[vertex.to_i] = neighbors.split(" ").each_with_object({}) do |pair, a|
        target, weight = pair.split(",")
        a[target.to_i] = weight.to_i
      end
    end
  end
end